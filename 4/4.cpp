#include<iostream>
using namespace std:
bool is_lucky_num(long long num){
	if(num % 7 != 0) return false;
	int seven =0, four = 0;
	while(num > 0){
		if(num % 10 == 7 ) seven ++;
		else if( num % 10 == 4) four ++;
		num /= 10;
	}
	if (seven < 3 || seven < four) return false;
	return true;
}

int main(void){
	int T;
	long long l,r;
	while(T --){
		long long lucky_num = 0;
		for(long long k = l; k <= r; k++)
			if(is_luck_num(k))
				lucky_num ++;
		cout << lucky_num <<endl;
	}
	return 0;
}