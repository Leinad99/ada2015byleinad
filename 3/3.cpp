/* 
function merge(), merge_sort() are modified from the code on this site:
	http://stackoverflow.com/questions/12030683/implementing-merge-sort-in-c 

function fastPower() is borrowed from the code of Mark Yan
*/

#include <iostream>
#include <algorithm>

using namespace std;

int fastPower(int b, int m, int n){ // borrowed from Mark Yan
	long long a = b;
	long long result = 1;
	while(n > 0){
		if(n & 1){ // n&1 == 0 even; n&1 == 1 odd
			result *= a;
			result %= m;
		}
		a *= a;
		a %= m;
		n /= 2;
	}
	return (int)(result % m);
}

/* i wins: c(i-j)(i+j)^e mod p > p/2 */
bool compare(int a, int b, int* DPtable, int c, long long e, int p){
	int f = (int)(e % (long long)(p-1)); // By Fermat's Little Theorem
	int remains = DPtable[a+b];
	remains = (int)(((long long)c * (long long)remains) % (long long)p);
	remains = (int)((long long)(a-b) * (long long)remains % (long long)p);  
	if(remains < 0) remains += p;
	
	return (remains > p/2)? true:false;
}

void merge(int low, int mid, int high, int* queue, int* DPtable, int c, long long e, int p){
	int* b = (int*)malloc((high - low + 1) * sizeof(int));
	int h = low, i = 0, j = mid+1;

	while((h <= mid) && (j <= high)){
		if(compare(queue[h], queue[j], DPtable, c, e, p)){
			b[i] = queue[h];
			h++;
		}else{
			b[i] = queue[j];
			j++;
		}
		i++;
	}
 
	if(h > mid){
		for(int k = j; k <= high; k++){
			b[i] = queue[k];
			i++;
		}
	}else{
		for(int k = h; k <= mid; k++){
			b[i] = queue[k];
			i++;
		}
 	}
 
	for(int k = low, l = 0; k <= high; k++, l++) 
		queue[k] = b[l];
}

void merge_sort(int low, int high, int* queue, int* DPtable, int c, long long e, int p){
	int mid;
	if(low < high){
		mid = low + (high - low)/2; //This avoids overflow when low, high are too large
		merge_sort(low, mid, queue, DPtable, c, e, p);
		merge_sort(mid+1, high, queue, DPtable, c, e, p);
		merge(low, mid, high, queue, DPtable, c, e, p);
	}
}

int main(){
	int T, n, c, p;
	long long e;
	cin >> T;
	
	for(int i = 0; i < T; i++){
		cin >> n >> c >> e >> p;
		int* queue = (int*)malloc(n * sizeof(int));
		for(int j = 0; j < n; j++) 
			queue[j] = j+1;

		int* DPtable = (int*)malloc(2 * n * sizeof(int));
		int f = (int)(e % (long long)(p-1));
		DPtable[0] = 0; DPtable[1] = 1;
		DPtable[2] = fastPower(2, p, f);
		int PN[7] = {2,3,5,7,11,13,17};
		for(int j = 3; j < 2 * n; j++){
			bool got = false;
			for(int k = 0; k < 7 && PN[k] < j; k++){
				if(j % PN[k] == 0){
					DPtable[j] = (int)(((long long)DPtable[PN[k]] * (long long)DPtable[j/PN[k]]) % p);
					got = true;
					break;
				}
			} 
			if(!got)
				DPtable[j] = fastPower(j, p, f);
		}
		
		merge_sort(0, n-1, queue, DPtable, c, e, p);
		for(int j = 0; j < n; j++) 
			cout << queue[j] << " ";
		cout << endl;
	}

	return 0;
}