#include <iostream>

using namespace std;

int main(){
	int T, x, a, b;
	cin >> T;
	for(int i = 0; i < T; i++){
		cin >> x;
		if(x < 1000)
			a = 0;
		else
			a = 1000;
		b = x - a;
		cout << a << " " << b << endl;
	}
	return 0;
}
