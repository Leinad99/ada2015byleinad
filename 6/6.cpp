#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

#define MAXN 50000
using namespace std;

typedef struct edge{
	int v1, v2;
	int weight;
	bool is_must;
} Edge;

bool cmp(const Edge& e1, const Edge& e2){ 
	return e1.weight < e2.weight; 
}

void redirect(int *head, int v){
	int son = v;
	int dad = head[v];
	int grandpa;
	bool again = 0;
	head[son] = son;
	do {
		if(head[dad] != dad){
			grandpa = head[dad];
			again = 1;
		}else{
			again = 0;
		}	
		head[dad] = son;
		son = dad;
		dad = grandpa;
	} while(again);
}

int main(){
	int T, n, m;
	cin >> T;
	for(int i = 0; i < T; i++){
		int must_num = 0;
		long long must_weight = 0; 
		cin >> n >> m;
		vector<Edge> Edges;
		map<int, Edge*> *vertex = new map<int, Edge*>[MAXN+1];
		int *head = new int[MAXN+1];
		for(int j = 0; j < n+1; j++) head[j] = j;
		for(int j = 0; j < m; j++){
			Edge tmp;
			cin >> tmp.v1 >> tmp.v2 >> tmp.weight;
			Edges.push_back(tmp);
		}
		sort(Edges.begin(), Edges.end(), cmp);
		vector<Edge>::iterator itr;
		/*for(itr = Edges.begin(); itr != Edges.end(); itr++)
			cout << (*itr).v1 <<" "<< (*itr).v2<<" "<< (*itr).weight << endl;*/
		int count = 0;
		bool to_stop = false;
		for(itr = Edges.begin(); !to_stop && itr != Edges.end(); itr++){
			bool cycle = false;
			int v1 = (*itr).v1;
			int v2 = (*itr).v2;
			/* Check cycle */
			vector<int> trace1, trace2;
			int again, now;
			now = v1;
			/* Find the ancient */
			trace1.push_back(v1);
			trace2.push_back(v2);
			do {
				//cout << "now: "<< now << ", head[now]: " << head[now] << endl;
				trace1.push_back(head[now]);
				if(head[now] != now){
					now = head[now];
					again = 1;
				}else
					again = 0;
			} while(again);
			now = v2;
			//cout << "lin be di jia!\n";
			do {
				//cout << "now: "<< now << ", head[now]: " << head[now] << endl;
				trace2.push_back(head[now]);
				if(head[now] != now){
					now = head[now];
					again = 1;
				}else
					again = 0;
			} while(again);
			//cout << "lin nia di jia!\n";
			/* Find cycle and deduct must */
			if(*(trace1.end()-1)  == *(trace2.end()-1)){ 
				//cout << "CYCLE!!!" <<"["<<v1<<"] "<<"["<<v2<<"] "<< endl;
				cycle = true;
				vector<int>::iterator t1, t2;
				for(t1 = trace1.end()-1, t2 = trace2.end()-1;;t1--, t2--){ // find the latest same head
					if(t1 == trace1.begin() || t2 == trace2.begin())
						break;
					else if(*(t1-1) != *(t2-1))
						break;
				}
				for( ;t1 != trace1.begin(); t1--){
					if(vertex[*t1][*(t1-1)]->weight == (*itr).weight
						&& vertex[*t1][*(t1-1)]->is_must == true){ // same level in the cycle
						vertex[*t1][*(t1-1)]->is_must = false;
						must_num--;
						must_weight -= vertex[*t1][*(t1-1)]->weight;
					}
				}
				for( ;t2 != trace2.begin(); t2--){
					if(vertex[*t2][*(t2-1)]->weight == (*itr).weight
						&& vertex[*t2][*(t2-1)]->is_must == true){ // same level in the cycle
						vertex[*t2][*(t2-1)]->is_must = false;
						must_num--;
						must_weight -= vertex[*t2][*(t2-1)]->weight;
					}
				}
			}
			if(cycle)
				continue;

			/* Join */
			if(head[v1] != v1)
				redirect(head, v1);
			if(head[v2] != v2)
				redirect(head, v2);
			head[v2] = v1;
			/* put into mst and set must */
			(*itr).is_must = true;
			//cout << "must_num(before): " << must_num << endl;
			//cout << "must_weight(before): " << must_weight << endl;
			must_num++;
			must_weight += (*itr).weight;
			//cout << "must_num(after): " << must_num << endl;
			//cout << "must_weight(after): " << must_weight << endl;
			count++;
			vertex[v1][v2] = &(*itr);
			vertex[v2][v1] = &(*itr);

			/* termination set */
			if(count >= n-1 && 
				itr+1 != Edges.end() && 
				(*(itr+1)).weight > (*itr).weight)
				to_stop = true;
		}
		cout << must_num << " " << must_weight << endl;
	}

	return 0;
}