#include <iostream>

using namespace std;

int main(){
	int T, n, c, lastc = 0;
	int s[10000] = {0}, digit = 0;

	cin >> T;
	
	for(int i = 0; i < T; i++){ // run T times
		cin >> n;

		for(int j = 0; j < digit; j++) // initialize
			s[j] = 0;
		lastc = 0;
		digit = 0;

		for(int j = 0; j < n; j++){ // run n days
			cin >> c;
			if(c > lastc){ // case 1
				int to_add = c - lastc; // to_add must > 0
				for(int k = 0;; k++){
					if(9 - s[k] >= to_add){ // s[k] + to_add <= 9
						s[k] += to_add;
						digit = (k+1 > digit)? k+1 : digit;
						break;
					}else{
						to_add -= (9 - s[k]);
						s[k] = 9;
					}
				}
			}else{
				if(c == lastc){
					if(s[0] < 9){
						s[0]++;
						lastc++;
					}else{
						s[0] = 0;
						lastc -= (9 - 1); // carry
						s[1]++;
						digit = (2 > digit)? 2 : digit; // k+2 because CARRY
						for(int k = 1; ; k++){
							if(s[k] == 10){
								s[k] = 0;
								lastc -= 9; // carry
								s[k+1]++;
								digit = (k+2 > digit)? k+2 : digit; // k+2 because CARRY								
							}else
								break;
						}
					}
				}

				int to_deduct = lastc - c;
				if(to_deduct > 0){
					for(int k = 0; to_deduct > 0 || s[k] == 10; k++){
						if(s[k] == 10){
							to_deduct -= 9;
						}else if(s[k] <= 9 && s[k] >= 0){ // ensuring digit
							to_deduct -= s[k]-1; // including CARRY
						}else{
							cout << endl << "fail: digit is not in range 0 to 9." << endl;
							return -1;
						}
						s[k] = 0;
						s[k+1]++;
						digit = (k+2 > digit)? k+2 : digit; // k+2 because CARRY
					}
				}

				int to_add_back = -to_deduct;
				for(int k = 0;; k++){
					if(9 - s[k] >= to_add_back){ // s[k] + to_add <= 9
						s[k] += to_add_back;
						digit = (k+1 > digit)? k+1 : digit;
						break;
					}else{
						to_add_back -= (9 - s[k]);
						s[k] = 9;
					}
				}
			}

			lastc = c;
			for(int k = digit-1; k >= 0; k--){
				cout << s[k];
			}
			if(j != n-1)	cout << " ";
		}
		cout << endl;
	}

	return 0;
}